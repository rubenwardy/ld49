extends Node


func _ready():
	$VBoxContainer/Start.grab_focus()

func _on_start_pressed():
	get_tree().change_scene("res://scenes/GameScene.tscn")

func _on_fullscreen():
	OS.window_fullscreen = !OS.window_fullscreen

func _on_exit():
	get_tree().quit()
