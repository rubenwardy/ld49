extends Vehicle

onready var turret = $Turret
var last_controller = 0
var last_mouse = 1
var last_mouse_position = Vector2()

var explosion_size = Vector2(128, 48)


func _get_input():
	var turn = Input.get_action_strength("steer_right") - Input.get_action_strength("steer_left")
	var power = Input.get_action_strength("accelerate") - Input.get_action_strength("brake")

	return {
		"turn": turn,
		"power": power,
	}


func _process(delta):
	if not enabled:
		turret.firing = false
		return

	var look_x = Input.get_action_strength("target_right") - Input.get_action_strength("target_left")
	var look_y = Input.get_action_strength("target_down") - Input.get_action_strength("target_up")
	var mouse_pos = get_local_mouse_position()

	if abs(look_x) > 0.1 or abs(look_y) > 0.1:
		last_controller = OS.get_ticks_msec() + 1000

	if last_mouse_position.distance_squared_to(mouse_pos) > 16:
		last_mouse = OS.get_ticks_msec()
		last_mouse_position = mouse_pos

	if last_controller > last_mouse:
		turret.target = global_position + 10000 * Vector2(look_x, look_y)
	else:
		turret.target = get_global_mouse_position()
	turret.firing = Input.is_action_pressed("fire")
