class_name Vehicle
extends KinematicBody2D

# Constants
export(float) var wheel_base = 54
export(float) var steering_angle = 15
export(float) var engine_power = 400
export(float) var friction = -0.9
export(float) var drag = -0.0015
export(float) var braking = -450
export(float) var max_speed_reverse = 250
export(float) var slip_speed = 400
export(float) var traction_fast = 0.3
export(float) var traction_slow = 0.9
export(float) var mass = 1000
export(float) var max_health = 500
export(bool) var enabled = true

onready var wheels = [ $WheelFL, $WheelFR, $WheelBL, $WheelBR ]

# Variables
var acceleration = Vector2.ZERO
var velocity = Vector2.ZERO
var steer_angle
onready var health = max_health


signal collided(collision, force)
signal health_changed(obj, health, delta)


func _get_input():
	push_error("Unimplemented: _get_input()")


func apply_input():
	var input = _get_input()
	steer_angle = input.turn * deg2rad(steering_angle)
	if input.power > 0:
		acceleration = transform.x * engine_power * input.power
	elif input.power < 0:
		acceleration = transform.x * braking * -input.power


func calculate_steering(delta):
	var rear_wheel = position - transform.x * wheel_base / 2.0
	var front_wheel = position + transform.x * wheel_base / 2.0
	rear_wheel += velocity * delta
	front_wheel += velocity.rotated(steer_angle) * delta
	var new_heading = (front_wheel - rear_wheel).normalized()
	var traction = traction_slow
	if velocity.length() > slip_speed:
		traction = traction_fast
	var d = new_heading.dot(velocity.normalized())
	if d > 0:
		velocity = velocity.linear_interpolate(new_heading * velocity.length(), traction)
	if d < 0:
		velocity = -new_heading * min(velocity.length(), max_speed_reverse)
	rotation = new_heading.angle()

	var wheel_angle = steer_angle if abs(steer_angle) > 0.1 else 0
	wheels[0].rotation = wheel_angle
	wheels[1].rotation = wheel_angle


func apply_friction():
	if velocity.length() < 5:
		velocity = Vector2.ZERO
	var friction_force = velocity * friction
	var drag_force = velocity * velocity.length() * drag
	if velocity.length() < 100:
		friction_force *= 3
	acceleration += drag_force + friction_force


func _physics_process(delta):
	if not enabled:
		return

	var velocity_before = velocity

	acceleration = Vector2.ZERO
	apply_input()
	apply_friction()
	calculate_steering(delta)
	velocity += acceleration * delta

	velocity = move_and_slide(velocity)

	var force = (mass * (velocity - velocity_before)).length() / delta
	var dmg = clamp(force / 15000000, 0, 1) / max(get_slide_count(), 1)
	if dmg > 0.1:
		for i in get_slide_count():
			var collision = get_slide_collision(i)
			print(name, " collision damage: ", dmg, ", force ", force)
			damage(dmg, collision.position)

			if collision.collider.has_method("damage"):
				collision.collider.damage(10 * dmg, collision.position)

			emit_signal("collided", collision, force / get_slide_count())


func damage(base_damage, hit_position):
	var damage = base_damage
	health -= damage
	emit_signal("health_changed", self, health, damage)
