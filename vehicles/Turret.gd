extends Node2D

# 540 per minute. Anywhere from 450 (7.5) to 1200 is realistic
export(int) var shots_per_second = 9
export(float) var spread = PI / 6.0
export(float) var base_damage_per_shot = 1
export(float) var gun_range = 200

var firing: bool = false
var target = null
var rounds_due: float = 0

onready var muzzleFlash: AnimatedSprite = $MuzzleFlash
onready var rayCast: RayCast2D = $RayCast
onready var BulletHole = preload("./BulletHole.tscn")
onready var ShotLine = preload("./ShotLine.tscn")

func _ready():
	rayCast.add_exception(get_parent())


func _process(delta: float) -> void:
	if target == null:
		return

	var target_angle = global_position.angle_to_point(target) - PI
	global_rotation = lerp_angle(global_rotation, target_angle, 15 * delta)

	if not firing:
		muzzleFlash.stop()
		muzzleFlash.hide()
		rounds_due = 0
		return

	if not muzzleFlash.playing:
		muzzleFlash.play()
		muzzleFlash.show()

	rounds_due += shots_per_second * delta
	while rounds_due > 0:
		rounds_due -= 1
		shoot()


func shoot():
	rayCast.enabled = true
	rayCast.cast_to = Vector2(0, gun_range)
	rayCast.rotation = ((randf() - 0.5) * spread - PI) / 2.0
	rayCast.force_raycast_update()

	var obj = rayCast.get_collider()

	if obj and obj.has_method("damage"):
		obj.damage(base_damage_per_shot, rayCast.get_collision_point())

	if obj:
		var hole = BulletHole.instance()
		obj.add_child(hole)
		hole.global_position = rayCast.get_collision_point()

	var ray_length = rayCast.get_collision_point().distance_to(rayCast.global_position) if obj else gun_range

	print("Instancing line under ", get_parent().get_parent().name)
	var shotLine = ShotLine.instance()
	shotLine.global_position = rayCast.global_position
	shotLine.global_rotation = rayCast.global_rotation + PI / 2.0
	var line: Line2D = shotLine.get_node("Line2D")
	line.points[1] = Vector2(ray_length, line.points[1].y)
	# Hack!
	get_parent().get_parent().add_child(shotLine)

	# Prevent updating during physics step
	rayCast.enabled = false


