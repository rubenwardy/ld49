extends Node2D

export(float) var lifetime = 20

func _process(delta):
	lifetime -= delta

	if lifetime < 0:
		queue_free()
