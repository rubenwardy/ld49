extends StaticBody2D


export(NodePath) var player_path = null
onready var player = get_node(player_path)
onready var turret = $Turret

var explosion_size = Vector2(16, 16)

var health = 50

signal health_changed(obj, health, delta)


func _process(delta):
	if not is_instance_valid(player):
		turret.firing = false
		return

	turret.target = player.global_position
	turret.firing = global_position.distance_squared_to(player.global_position) < 400*400


func damage(base_dmg, hit_position):
	assert(self != null)
	health -= base_dmg
	emit_signal("health_changed", self, health, base_dmg)

