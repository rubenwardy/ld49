extends Vehicle


export(NodePath) var player_path = null
export(NodePath) var trigger_path = null

onready var player = get_node(player_path)
onready var trigger = get_node(trigger_path) if trigger_path else null
onready var turret = $Turret
onready var rayForward = $RayForward
var reverse_started: float = -1.0


var explosion_size = Vector2(61, 32)


func _ready():
	if trigger:
		enabled = false
		trigger.connect("body_entered", self, "_on_triggered")


func _get_input():
	if not is_instance_valid(player):
		return { "turn": 0, "power": 0 }

	var current_msec = OS.get_system_time_msecs()
	if rayForward.is_colliding() and reverse_started == -1:
		reverse_started = current_msec

	var sqDistToPlayer = global_position.distance_squared_to(player.global_position)
	var directionToPlayer = global_position.direction_to(player.global_position)
	var direction = Vector2.RIGHT.rotated(rotation)
	var angle = direction.angle_to(directionToPlayer)
	var turn = clamp(6 * angle / PI, -1, 1)
	var power = 0.8 if sqDistToPlayer < turret.gun_range * turret.gun_range else 1

	if current_msec - reverse_started < 2000:
		return { "turn": -turn, "power": -0.7 }
	else:
		reverse_started = -1

	return { "turn": turn, "power": power }


func _process(delta):
	if not is_instance_valid(player):
		turret.firing = false
		return

	turret.target = player.global_position
	turret.firing = global_position.distance_squared_to(player.global_position) < 50000


func _on_triggered(node):
	print("Enemy activated")
	enabled = true
