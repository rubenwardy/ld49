extends Node2D

export(Vector2) var extents

onready var fire: CPUParticles2D = $Fire
onready var smoke = $Smoke

func _ready():
	fire.emission_rect_extents = extents
	fire.restart()
	smoke.restart()

func _process(delta):
	if not (fire.emitting or smoke.emitting):
		print("Explosion done")
		queue_free()

