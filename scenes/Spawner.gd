extends Area2D

onready var Enemy = preload("../vehicles/Enemy.tscn")
onready var spawn_point = $SpawnPoint

func _on_body_entered(body):
	var enemy = Enemy.instance()
	enemy.global_position = spawn_point.global_position
	enemy.global_rotation = spawn_point.global_rotation
	enemy.player_path = "../Player"
	enemy.enabled = true
	get_parent().add_child(enemy)
	enemy.connect("health_changed", get_parent(), "_on_any_health_changed")
	queue_free()
