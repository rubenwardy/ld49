extends Node2D

onready var player = $Player
onready var danger_gauge = $HUD/DangerGauge
onready var game_over = $HUD/GameOver
onready var success = $HUD/Success
onready var Explosion = preload("./Explosion.tscn")


func _on_player_health_changed(player: Vehicle, health: float, delta: float):
	danger_gauge.max_value = player.max_health
	danger_gauge.add_damage(delta)
	_on_any_health_changed(player, health, delta)


func _on_any_health_changed(node, health: float, delta: float):
	if health <= 0:
		explode(node)


func explode(node):
	assert(node != null)
	var explosion = Explosion.instance()
	explosion.global_rotation = node.global_rotation
	explosion.global_position = node.global_position
	explosion.extents = node.explosion_size
	add_child(explosion)

	node.queue_free()


func _on_finish_line_entered(body: Vehicle):
	success.show()
	body.enabled = false


func _on_player_died():
	if is_instance_valid(player):
		game_over.show()
		danger_gauge.hide()
		explode(player)


func _on_player_collided(collision, force):
	var val = clamp(force / 300000, 0, 10)
	print("Shock ", val, " force ", force)
	danger_gauge.add_shock(val)
