extends TileSet
tool

onready var grasses = [ find_tile_by_name("Grass"), find_tile_by_name("GrassShort"), find_tile_by_name("GrassLong") ]

func _is_tile_bound(drawn_id, neighbour_id):
	var connect_to = [ find_tile_by_name("Wall"),  find_tile_by_name("Sign") ]
	var grasses = [ find_tile_by_name("Grass"), find_tile_by_name("GrassShort"), find_tile_by_name("GrassLong") ]
	return (neighbour_id in connect_to or neighbour_id in grasses) and drawn_id in grasses
