extends ColorRect


func _ready():
	hide()


func _on_try_again_pressed():
	get_tree().change_scene("res://scenes/GameScene.tscn")


func _on_mainmenu_pressed():
	get_tree().change_scene("res://MainMenu.tscn")
