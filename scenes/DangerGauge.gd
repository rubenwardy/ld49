extends Control

onready var needle = $BG/Needle

var max_value = 100
var current_damage = 0
var current_shock = 0

signal max_exceeded


func update() -> void:
	var current = current_damage + current_shock
	var perc = clamp(current / max_value, 0, 1)
	needle.rect_rotation = (2.0 * perc - 1) * 131.0


func add_damage(points: float) -> void:
	current_damage += points
	current_shock += 2 * points

	if current_damage + current_shock > max_value:
		emit_signal("max_exceeded")

	update()


func add_shock(points: float) -> void:
	current_shock += points
	if current_damage + current_shock > max_value:
		emit_signal("max_exceeded")

	update()


func _process(delta) -> void:
	if current_shock <= 0:
		current_shock = 0

	current_shock -= 10 * delta
	update()
