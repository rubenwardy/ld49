extends StaticBody2D

var health = 20

func damage(base_dmg, hit_position):
	health -= base_dmg
	if health < 0:
		queue_free()
